import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { AuthService } from 'src/app/shared/services/authservice/auth.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  signupForm: FormGroup
  isValidFormSubmitted = null


  constructor(public fb: FormBuilder, public authService: AuthService, public router: Router) {
    this.signupForm = this.fb.group({
      username: [''],
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')
      ]),
      password: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(8)]),
      birthdate: new FormControl('', [Validators.required])
    })
  }

  ngOnInit(): void {
    
  }

  register() {
    this.isValidFormSubmitted = false
    console.log(this.signupForm.value)
    if (this.signupForm.invalid) {
      return
    }
    this.isValidFormSubmitted = true
    this.authService.signUp(this.signupForm.value).subscribe(res => {
      if (res.result) {
        this.signupForm.reset()
        this.router.navigate(['/login'])
      }
    })
  }
}
