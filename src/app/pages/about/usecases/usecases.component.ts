import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Sub maken',
      description: 'Hiermee kan een gebruiker een subforum aanmaken',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Subforum aanmaken en vult gegevens in en klikt op aanmaken.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het niewe aangemaakte subforum.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Subforum is aangemaakt',
    },
    {
      id: 'UC-03',
      name: 'Post maken',
      description: 'Hiermee kan een gebruiker een post aanmaken',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Post aanmaken en vult gegevens in en klikt op aanmaken.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar de niewe aangemaakte post'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Post is aangemaakt',
    },
    {
      id: 'UC-04',
      name: 'Comment plaatsen',
      description: 'Hiermee kan een gebruiker een comment plaatsen',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Reply en vult gegevens in en klikt op aanmaken.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar de niewe aangemaakte comment'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Comment is aangemaakt',
    },
    {
      id: 'UC-05',
      name: 'Post verwijderen',
      description: 'Hiermee kan een gebruiker een post verwijderen',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Post verwijderen en verifieert nogmaals dat de post verwijderd moet worden.',
        'De applicatie valideert of de gebruiker de post heeft geplaatst.',
        'Indien gegevens correct zijn dan laat de applicatie een melding zien dat de post is verwijderd.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd, en de post behoort tot de gebruiker.',
      postcondition: 'Post is verwijderd',
    },
    {
      id: 'UC-06',
      name: 'Comment verwijderen',
      description: 'Hiermee kan een gebruiker een comment verwijderen',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Comment verwijderen en verifieert nogmaals dat de comment verwijderd moet worden.',
        'De applicatie valideert of de gebruiker de comment heeft geplaatst.',
        'Indien gegevens correct zijn dan laat de applicatie een melding zien dat de comment is verwijderd.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd, en de comment behoort tot de gebruiker.',
      postcondition: 'Comment is verwijderd',
    },
    {
      id: 'UC-07',
      name: 'Sub verwijderen',
      description: 'Hiermee kan een gebruiker een Subforum verwijderen, indien hij de sub heeft aangemaakt of Administrator is.',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Subforum verwijderen en verifieert nogmaals dat het Subforum verwijderd moet worden.',
        'De applicatie valideert of de gebruiker het subforum heeft aangemaakt, of dat hij een administrator is.',
        'Indien gegevens correct zijn dan laat de applicatie een melding zien dat het subforum is verwijderd.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd, en het subforum behoort tot de gebruiker, of de gebruiker is een Administrator',
      postcondition: 'Subforum is verwijderd',
    },
    {
      id: 'UC-08',
      name: 'Post aanpassen',
      description: 'Hiermee kan een gebruiker een post aanpassen',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Post aanpassen en vult gegevens in en klikt op aanpassen.',
        'De applicatie valideert of de gebruiker de post heeft geplaatst.',
        'Indien gegevens correct zijn dan laat de applicatie een melding zien dat de post is aangepast'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd, en de post behoort tot de gebruiker.',
      postcondition: 'Post is aangepast',
    },
    {
      id: 'UC-09',
      name: 'Comment aanpassen',
      description: 'Hiermee kan een gebruiker een comment aanpassen',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Comment aanpassen en vult gegevens in en klikt op aanpassen.',
        'De applicatie valideert of de gebruiker de comment heeft geplaatst.',
        'Indien gegevens correct zijn dan laat de applicatie een melding zien dat de comment is aangepast'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd, en de comment behoort tot de gebruiker.',
      postcondition: 'Comment is aangepast',
    },
    {
      id: 'UC-10',
      name: 'Subforum aanpassen',
      description: 'Hiermee kan een gebruiker een Subforum aanpassen',
      scenario: [
        'Ingelogde gebruiker klikt op de knop Subforum aanpassen en vult gegevens in en klikt op aanpassen.',
        'De applicatie valideert of de gebruiker het subforum heeft aangemaakt.',
        'Indien gegevens correct zijn dan laat de applicatie een melding zien dat het subforum is aangepast'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd, en het Subforum behoort tot de gebruiker.',
      postcondition: 'Subforum is aangepast',
    },
    {
      id: 'UC-11',
      name: 'Post bekijken',
      description: 'Hiermee kan een gebruiker een post bekijken',
      scenario: [
        'Ingelogde gebruiker klikt op de desbetreffende post',
        'De applicatie valideert of de gebruiker ingelogd is of niet en laat op basis daarvan wel of niet toe dat ze een comment kunnen plaatsen of liken.',
        'De applicate laat de post zien.'],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'Post wordt weergegeven',
    },
    {
      id: 'UC-12',
      name: 'Comments bekijken',
      description: 'Hiermee kan een gebruiker de comments onder een post bekijken.',
      scenario: [
        'Ingelogde gebruiker klikt op de desbetreffende post',
        'De applicatie valideert of de gebruiker ingelogd is of niet en laat op basis daarvan wel of niet toe dat ze een comment kunnen plaatsen of liken.',
        'De applicate laat de post zien, en de comments op de post daaronder.'],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'Comments worden weergegeven',
    },
    {
      id: 'UC-13',
      name: 'Subforum bekijken',
      description: 'Hiermee kan een gebruiker een subforum bekijken.',
      scenario: [
        'Ingelogde gebruiker klikt op het desbetreffende subforum',
        'De applicatie valideert of de gebruiker ingelogd is of niet en laat op basis daarvan wel of niet toe dat ze een post kunnen plaatsen of liken.',
        'De applicate laat het subforum zien, en posts in het subforum.'],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'Subforum wordt weergegeven',
    },
    {
      id: 'UC-14',
      name: 'Post liken/disliken',
      description: 'Hiermee kan een gebruiker een post liken/upvoten of disliken',
      scenario: [
        'Ingelogde gebruiker klikt op het hartje/pijltje bij desbetreffende post',
        'De applicatie valideert of de gebruiker ingelogd is of niet en laat op basis daarvan wel of niet toe dat ze een post kunnen liken.',
        'De applicate veranderd de kleur van het hartje/pijltje, en laat daarmee zien dat de user de post heeft geliked/disliked'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Post is geliked/geupvote',
    },
    {
      id: 'UC-15',
      name: 'Comment liken/disliken',
      description: 'Hiermee kan een gebruiker een comment liken/upvoten of disliken',
      scenario: [
        'Ingelogde gebruiker klikt op het hartje/pijltje bij desbetreffende comment',
        'De applicatie valideert of de gebruiker ingelogd is of niet en laat op basis daarvan wel of niet toe dat ze een comment kunnen liken.',
        'De applicate veranderd de kleur van het hartje/pijltje, en laat daarmee zien dat de user de comment heeft geliked/disliked'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Comment is geliked/geupvote',
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
