import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { SubForumService } from 'src/app/pages/subforum/subforum.service'
import { AuthService } from 'src/app/shared/services/authservice/auth.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-subforum-create',
  templateUrl: './subforum-create.component.html',
  styleUrls: ['./subforum-create.component.css']
})
export class SubForumCreateComponent implements OnInit {
  createForm: FormGroup
  isValidFormSubmitted = true

  constructor(public fb: FormBuilder, public subForumService: SubForumService, public authService: AuthService, public router: Router) {
    this.createForm = this.fb.group({
      name: new FormControl('', [
        Validators.required,
      ]),
      about: new FormControl('', [
        Validators.required,
      ]),
      rules: new FormControl('',
        [Validators.required
        ]),
      admin: new FormControl('')
    })
  }

  ngOnInit(): void {

  }

  create() {
    if (!this.authService.isLoggedIn) {
      this.isValidFormSubmitted = false
      return
    } else {
      this.createForm.value.admin = localStorage.getItem('user_id')
    }
    console.log(this.createForm.value)
    if (this.createForm.invalid) {
      this.isValidFormSubmitted = false
      return
    }
    this.isValidFormSubmitted = true
    this.subForumService.create(this.createForm.value).subscribe(res => {
      if (res.result.id) {
        this.createForm.reset()
        this.router.navigate(['/subs'])
      }
    })
  }

}
