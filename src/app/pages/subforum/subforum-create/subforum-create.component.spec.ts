import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubForumCreateComponent } from './subforum-create.component';

describe('SubforumCreateComponent', () => {
  let component: SubForumCreateComponent;
  let fixture: ComponentFixture<SubForumCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubForumCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubForumCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
