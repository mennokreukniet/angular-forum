import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubforumUpdateComponent } from './subforum-update.component';

describe('SubforumUpdateComponent', () => {
  let component: SubforumUpdateComponent;
  let fixture: ComponentFixture<SubforumUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubforumUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubforumUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
