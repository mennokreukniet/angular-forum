import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { SubForumService } from 'src/app/pages/subforum/subforum.service'
import { AuthService } from 'src/app/shared/services/authservice/auth.service'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { Sub } from '../sub.model';

@Component({
  selector: 'app-subforum-update',
  templateUrl: './subforum-update.component.html',
  styleUrls: ['./subforum-update.component.css']
})
export class SubforumUpdateComponent implements OnInit {

  updateForm: FormGroup
  isValidFormSubmitted = true
  subId: string;
  sub: [];

  constructor(public fb: FormBuilder, public subForumService: SubForumService, public authService: AuthService, public router: Router, private route: ActivatedRoute) {
    this.updateForm = this.fb.group({
      name: new FormControl('', [
        Validators.required,
      ]),
      about: new FormControl('', [
        Validators.required,
      ]),
      rules: new FormControl('',
        [Validators.required
        ]),
      admin: new FormControl('')
    })
  }

  ngOnInit(): void {
    this.subId = this.route.snapshot.params['id'];
  }

  update() {
    if (!this.authService.isLoggedIn) {
      this.isValidFormSubmitted = false
      return
    } else {
      this.updateForm.value.admin = localStorage.getItem('user_id')
    }
    console.log(this.updateForm.value)
    if (this.updateForm.invalid) {
      this.isValidFormSubmitted = false
      return
    }
    this.isValidFormSubmitted = true
    this.subForumService.update(this.subId, this.updateForm.value).subscribe(res => {
      console.log(res)
      if (res.message) {
        this.updateForm.reset()
        this.router.navigate(['/subs'])
      }
    })
  }

}
