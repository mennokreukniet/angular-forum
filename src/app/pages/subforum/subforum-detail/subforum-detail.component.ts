import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Sub } from 'src/app/pages/subforum/sub.model';
import { SubForumService } from '../subforum.service';
import { AuthService } from 'src/app/shared/services/authservice/auth.service';
import * as internal from 'events';
import { collapseTextChangeRangesAcrossMultipleVersions } from 'typescript';
import { Router } from '@angular/router'

@Component({
  selector: 'app-subforum-detail',
  templateUrl: './subforum-detail.component.html',
  styleUrls: ['./subforum-detail.component.css']
})
export class SubForumDetailComponent implements OnInit {
  sub$: Observable<Sub>;
  subAdmin: string;
  user_id: string;
  subId: string;
  sub: Sub[];
  isAdmin: boolean;

  constructor(
    private subForumService: SubForumService,
    private route: ActivatedRoute,
    public authService: AuthService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.sub$ = this.route.paramMap.pipe(
      tap((params: ParamMap) => console.log('sub._id = ', params.get('id'))),
      switchMap((params: ParamMap) =>
        this.subForumService.getById(params.get('id'))
      ),
      tap(console.log)
    );

    this.subId = this.route.snapshot.params['id'];

    console.log(this.subId)
    this.setSubAdmin()

    if (this.user_id == this.subAdmin) {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
    console.log(this.isAdmin)
  }

  setSubAdmin(): void {
    this.subForumService.getById(this.subId)
      .subscribe(
        data => {
          console.log(data.admin);
          this.subAdmin = data.admin
        },
        error => {
          console.log(error);
        });
  }

  delete() {
    this.subForumService.delete(this.subId)

    this.router.navigate(['/subs'])
  }

  update() {
    this.router.navigate(['/sub/' + this.subId + '/update'])
  }

}
