import { Entity } from '../../shared/services/entityservice/entity.model';

export class Sub extends Entity {
    name: string;
    about: string;
    rules: string;
    admin: string;
}