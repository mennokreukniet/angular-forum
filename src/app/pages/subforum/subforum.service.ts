import { Injectable } from '@angular/core';
import { from, Observable, of, throwError } from 'rxjs';
import { delay, filter, mergeMap, take } from 'rxjs/operators';
import { catchError, map } from 'rxjs/operators'
import { Sub } from 'src/app/pages/subforum/sub.model';
import { EntityService } from 'src/app/shared/services/entityservice/entity.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root',
})
export class SubForumService extends EntityService<Sub> {
  headers = new HttpHeaders().set('Content-Type', 'application/json')

  constructor(http: HttpClient, public router: Router) {
    super(http, environment.SERVER_API_URL, 'sub');
  }

  create(sub: Sub): Observable<any> {
    const api = environment.SERVER_API_URL + 'sub'
    return this.http.post(api, sub).pipe(catchError(this.handleError))
  }

  update(id: string, sub: Sub): Observable<any> {
    const headers = { 'content-type': 'application/json' };
    const apiUrl = environment.SERVER_API_URL + 'sub/' + id
    return this.http.put<any>(apiUrl, sub, { headers });
  }

  getSub(id): Observable<any> {
    const api = environment.SERVER_API_URL + 'sub/' + id
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.handleError)
    )
  }

  delete(subId) {
    const endpoint = environment.SERVER_API_URL + 'sub/' + subId
    this.http.delete(endpoint).subscribe(data => {
      console.log(data);
    });
  }
}
