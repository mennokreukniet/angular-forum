import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Sub } from 'src/app/pages/subforum/sub.model';
import { SubForumService } from '../subforum.service';
import { AuthService } from 'src/app/shared/services/authservice/auth.service';

@Component({
  selector: 'app-subforum',
  templateUrl: './subforum.component.html',
  styleUrls: ['./subforum.component.css']
})
export class SubForumComponent implements OnInit {

  subs$: Observable<Sub[]>;
  isLoggedIn$: boolean;

  constructor(private subForumService: SubForumService, public authService: AuthService) {}
  
  ngOnInit(): void {
    console.log('Subforum list loaded');
    this.subs$ = this.subForumService.list();
    this.isLoggedIn$ = this.authService.isLoggedIn;
  }
  

}
