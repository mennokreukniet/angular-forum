import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { LayoutComponent } from './layout/layout.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SubForumComponent } from './pages/subforum/subforum-list/subforum.component';
import { SubForumDetailComponent } from './pages/subforum/subforum-detail/subforum-detail.component'
import { SubForumCreateComponent } from './pages/subforum/subforum-create/subforum-create.component'
import { SubforumUpdateComponent } from './pages/subforum/subforum-update/subforum-update.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
      { path: 'subs', pathMatch: 'full', component: SubForumComponent },
      { path: 'subs/:id', pathMatch: 'full', component: SubForumDetailComponent },
      { path: 'sub/create', pathMatch: 'full', component: SubForumCreateComponent },
      { path: 'sub/:id/update', pathMatch: 'full', component: SubforumUpdateComponent },
      { path: 'about', pathMatch: 'full', component: UsecasesComponent },
      {
        path: 'users',
        loadChildren: () =>
          import('./pages/user/user.module').then((m) => m.UserModule),
      },
      { path: 'login', pathMatch: 'full', component: LoginComponent },
      { path: 'register', pathMatch: 'full', component: RegisterComponent },
    ],
  },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
