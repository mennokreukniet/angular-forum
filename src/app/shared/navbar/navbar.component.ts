import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { AuthService } from 'src/app/shared/services/authservice/auth.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  isNavbarCollapsed = true;

  constructor(public authService: AuthService) {}
  
  logout() {
    this.authService.doLogout() 
  }

  ngOnInit(): void {}
}
