export abstract class Entity {
    readonly id!: number | undefined;

    constructor(values: any){
        this.id = values ? values.id : undefined;
    }
}