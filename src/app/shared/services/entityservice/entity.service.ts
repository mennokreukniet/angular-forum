import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { tap, map } from 'rxjs/operators';
import { Entity } from './entity.model'
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export abstract class EntityService<T extends Entity> {
    constructor(
        public readonly http: HttpClient,
        public readonly url: string,
        public readonly endpoint: string
    ) { }

    public list(params?: HttpParams): Observable<T[] | null> {
        const endpoint = `${this.url}${this.endpoint}`;
        return this.http
            .get<T[]>(endpoint, { params, observe: 'response' })
            .pipe(
                tap(console.log),
                map((response) => response.body),
                catchError(this.handleError)
            );
    }

    public getById(id: string, params?: HttpParams): Observable<T | null> {
        const endpoint = `${this.url}${this.endpoint}` + '/' + id;
        return this.http
            .get<T[]>(endpoint, { params, observe: 'response' })
            .pipe(
                tap(console.log),
                map((response) => response.body),
                catchError(this.handleError)
            );
    }

    /*handleError(handleError: any): import("rxjs").OperatorFunction<any, any> {
        throw new Error('Method not implemented.');
    }*/
    
    handleError(error: HttpErrorResponse) {
        let msg = ''
        if (error.error instanceof ErrorEvent) {
          // client-side error
          msg = error.error.message
        } else {
          // server-side error
          msg = `Error Code: ${error.status}\nMessage: ${error.message}`
        }
        return throwError(msg)
      }
}